import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget headSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'NONTHAWAT NILSONTHI',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
            ],
      ),
    );
    Widget infoSection = Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    'Information',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
              Container(
                  padding: const EdgeInsets.only(left: 40),
                  child: Text(
                    '- Date of birth: 19 October 1999',
                  )),
              Container(
                  padding: const EdgeInsets.only(left: 40),
                  child: Text(
                    '- Age: 21 years old',
                  )),
              Container(
                  padding: const EdgeInsets.only(left: 40),
                  child: Text(
                    '- Gender: Male',
                  )),
            ],
          )),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Container(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      'Contact',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
                Container(
                    padding: const EdgeInsets.only(left: 40),
                    child: Text(
                      '- Address: 460/41 M.3 Nong Kham, Sriracha, Chon Buri 20110',
                    )),
                Container(
                    padding: const EdgeInsets.only(left: 40),
                    child: Text(
                      '- Email: nonthawat.nst@gmail.com',
                    )),
                Container(
                    padding: const EdgeInsets.only(left: 40),
                    child: Text(
                      '- Phone: 083-014-0814',
                    )),
              ]))
        ],
      ),
    );
    Widget skillSection1 = Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              'images/python.png',
              width: 75,
              height: 75,
            ),
            Image.asset(
              'images/java.png',
              width: 75,
              height: 75,
            ),
            Image.asset(
              'images/unix.png',
              width: 75,
              height: 75,
            ),
          ],
        ));
    Widget skillSection2 = Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              'images/html.png',
              width: 75,
              height: 75,
            ),
            Image.asset(
              'images/css.png',
              width: 75,
              height: 75,
            ),
            Image.asset(
              'images/js.png',
              width: 75,
              height: 75,
            ),
          ],
        ));
    Widget skillSection3 = Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              'images/vue.jpeg',
              width: 75,
              height: 75,
            ),
            Image.asset(
              'images/docker.png',
              width: 75,
              height: 75,
            ),
            Image.asset(
              'images/mysql.jpg',
              width: 75,
              height: 75,
            ),
          ],
        ));
    Widget eduSection = Container(
       padding: EdgeInsets.all(10),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      'EDUCATION',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 40),
                    child: Text(
                      '2018 - Present:  B.B.A (Computer Science) Faculty of Informatics Burapha University - in progress ',
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 40),
                    child: Text(
                        '2016 - 2018:       High School Certificate : Sriracha School'),
                  ),
                ],
              ),
            )
          ],
        )

    );
    Widget hobbySection = Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Expanded(
            child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Container(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      'Hobby',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )),
                Container(
                    padding: const EdgeInsets.only(left: 40),
                    child: Text(
                      '- Play Valorant Game',
                    )),
                Container(
                    padding: const EdgeInsets.only(left: 40),
                    child: Text(
                      '- Straming on Twitch Platform',
                    )),
              ])
          )
        ],
      ),
    );
    Widget textSkill = Container(
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text(
              'Skill',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),

    );
    return MaterialApp(
      title: 'Resume',
      home: Scaffold(
          appBar: AppBar(
            title: const Text('Resume'),
          ),
          body: ListView(
            padding: EdgeInsets.all(10),
            children: [
              Image.asset(
                'images/profile.jpg',
                width: 150,
                height: 150,
              ),
              headSection,
              infoSection,
              textSkill,
              skillSection1,
              skillSection2,
              skillSection3,
              eduSection,
              hobbySection,
            ],
          )),
    );
  }
}
